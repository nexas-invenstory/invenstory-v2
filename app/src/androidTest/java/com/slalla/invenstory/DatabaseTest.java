package com.slalla.invenstory;

import android.content.Context;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Room;
import androidx.room.Update;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.slalla.invenstory.database.InvenstoryDatabase;
import com.slalla.invenstory.database.dao.CollectionDao;
import com.slalla.invenstory.database.dao.ItemDao;
import com.slalla.invenstory.database.entity.Collection;
import com.slalla.invenstory.database.entity.Item;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTest {
    private InvenstoryDatabase db;
    private CollectionDao collectionDao;
    private ItemDao itemDao;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db = Room.inMemoryDatabaseBuilder(context, InvenstoryDatabase.class).allowMainThreadQueries().build();
        collectionDao = db.collectionDao();
        itemDao = db.itemDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    //Required to allow running on background thread.
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Test
    public void manageCollection() throws Exception{
        //Insert Collection
        Collection insertCollection = new Collection();
        String insertCollectionDescription = "A collection of video games";
        String collectionName = "Video Games";
        insertCollection.setDescription(insertCollectionDescription);
        insertCollection.setName(collectionName);

        long insertId = collectionDao.insertCollection(insertCollection);
        assertNotEquals(-1, insertId);

        //Get Collection By Id
        Collection getCollectionById = LiveDataTestUtil.getOrAwaitValue(collectionDao.getCollectionById(insertId));
        assertEquals(insertId, getCollectionById.getId());

        //Insert Collections
        Collection insertCollection2 = new Collection();
        String insertCollectionDescription2 = "A collection of shoes";
        String collectionName2 = "Shows";
        insertCollection2.setDescription(insertCollectionDescription2);
        insertCollection2.setName(collectionName2);

        Collection insertCollection3 = new Collection();
        String insertCollectionDescription3 = "A collection books";
        String collectionName3 = "Books";
        insertCollection3.setDescription(insertCollectionDescription3);
        insertCollection3.setName(collectionName3);

        Collection insertCollection4 = new Collection();
        String insertCollectionDescription4 = "A collection trading cards";
        String collectionName4 = "Cards";
        insertCollection4.setDescription(insertCollectionDescription4);
        insertCollection4.setName(collectionName4);

        List<Long> ids = collectionDao.insertCollections(new Collection[]{insertCollection2, insertCollection3, insertCollection4});
        for(long id : ids){
            assertNotEquals(-1, id);
        }

        //Get All Collections
        List<Collection> getAllCollections = LiveDataTestUtil.getOrAwaitValue(collectionDao.getAllCollections());
        assertEquals(4, getAllCollections.size());

        //Update Collection
        String updatedDescription = "A collection of books";
        insertCollection3.setId(ids.get(1));
        insertCollection3.setDescription(updatedDescription);

        int updated = collectionDao.updateCollection(insertCollection3);
        assertEquals(1, updated);

        //Update Collections
        String updatedDescription2 = "A collection of trading cards";
        insertCollection4.setId(ids.get(2));
        insertCollection4.setDescription(updatedDescription);
        String updatedName = "Shoes";
        insertCollection2.setName(updatedName);
        insertCollection2.setId(ids.get(0));

        updated = collectionDao.updateCollections(new Collection[]{insertCollection2, insertCollection4});
        assertEquals(2, updated);

        //Get Collections With Names Like
        String search = "oo";
        List<Collection> collectionsWithNamesLike = LiveDataTestUtil.getOrAwaitValue(collectionDao.getCollectionsWithNamesLike(search));
        for(Collection c : collectionsWithNamesLike) {
            assertTrue(c.getName().contains(search));
        }

        //Get Collections Starting With
        String search2 = "S";
        List<Collection> collectionsStartingWith = LiveDataTestUtil.getOrAwaitValue(collectionDao.getCollectionsStartingWith(search2));
        for(Collection c : collectionsStartingWith) {
            assertTrue(c.getName().startsWith(search2));
        }

        //Delete Collection
        collectionDao.deleteCollection(getCollectionById);
        getAllCollections = LiveDataTestUtil.getOrAwaitValue(collectionDao.getAllCollections());
        assertEquals(3, getAllCollections.size());
        for(Collection c : getAllCollections){
            assertNotEquals(c.getId(), insertId);
        }

        //Delete Collections
        collectionDao.deleteCollections(new Collection[]{getAllCollections.get(0), getAllCollections.get(1)});
        long removedId1 = getAllCollections.get(0).getId();
        long removedId2 = getAllCollections.get(1).getId();
        getAllCollections = LiveDataTestUtil.getOrAwaitValue(collectionDao.getAllCollections());
        assertEquals(1, getAllCollections.size());
        for(Collection c : getAllCollections){
            assertNotEquals(c.getId(), removedId1);
            assertNotEquals(c.getId(), removedId2);
        }

        //Delete All Collections
        collectionDao.deleteAllCollections();
        getAllCollections = LiveDataTestUtil.getOrAwaitValue(collectionDao.getAllCollections());
        assertEquals(0, getAllCollections.size());
    }

    @Test
    public void manageItem() throws Exception{
        //Insert Item
        String collectionName = "Movies";
        String collectionDescription = "A collection of movies";
        Collection collection = insertCollection(collectionName, collectionDescription);
        String collectionName2 = "DC Movies";
        String collectionDescription2 = "A collection of DC movies";
        Collection collection2 = insertCollection(collectionName2, collectionDescription2);

        Item insertItem = new Item();
        String insertItemName = "Black Widow";
        String insertItemDescription = "A 2021 film on the origins of Black Widow.";
        float insertItemPrice = 29.99f;
        float insertItemValue = 10.00f;
        insertItem.setName(insertItemName);
        insertItem.setDescription(insertItemDescription);
        insertItem.setPrice(insertItemPrice);
        insertItem.setValue(insertItemValue);
        insertItem.setCollectionId(collection.getId());

        long insertId = itemDao.insertItem(insertItem);
        assertNotEquals(-1, insertId);

        //Insert Items
        Item insertItem2 = new Item();
        String insertItemName2 = "Shang Chi";
        String insertItemDescription2 = "A 2021 film on the origins of Sang Chi.";
        float insertItemPrice2 = 29.99f;
        float insertItemValue2 = 10.00f;
        insertItem2.setName(insertItemName2);
        insertItem2.setDescription(insertItemDescription2);
        insertItem2.setPrice(insertItemPrice2);
        insertItem2.setValue(insertItemValue2);
        insertItem2.setCollectionId(collection.getId());

        Item insertItem3 = new Item();
        String insertItemName3 = "Avengers: Edgame";
        String insertItemDescription3 = "A 2019 film concluding The Infinity Saga";
        float insertItemPrice3 = 29.99f;
        float insertItemValue3 = 10.00f;
        insertItem3.setName(insertItemName3);
        insertItem3.setDescription(insertItemDescription3);
        insertItem3.setPrice(insertItemPrice3);
        insertItem3.setValue(insertItemValue3);
        insertItem3.setCollectionId(collection.getId());

        Item insertItem4 = new Item();
        String insertItemName4 = "Shazam";
        String insertItemDescription4 = "One of the only good DC films";
        float insertItemPrice4 = 29.99f;
        float insertItemValue4 = 10.00f;
        insertItem4.setName(insertItemName4);
        insertItem4.setDescription(insertItemDescription4);
        insertItem4.setPrice(insertItemPrice4);
        insertItem4.setValue(insertItemValue4);
        insertItem4.setCollectionId(collection2.getId());

        Item insertItem5 = new Item();
        String insertItemName5 = "Shazam";
        String insertItemDescription5 = "One of the only good DC films";
        float insertItemPrice5 = 29.99f;
        float insertItemValue5 = 10.00f;
        insertItem5.setName(insertItemName5);
        insertItem5.setDescription(insertItemDescription5);
        insertItem5.setPrice(insertItemPrice5);
        insertItem5.setValue(insertItemValue5);
        insertItem5.setCollectionId(collection2.getId());

        Item insertItem6 = new Item();
        String insertItemName6 = "Captain Marvel";
        String insertItemDescription6 = "The origin story of Captain Marvel.";
        float insertItemPrice6 = 29.99f;
        float insertItemValue6 = 10.00f;
        insertItem6.setName(insertItemName6);
        insertItem6.setDescription(insertItemDescription6);
        insertItem6.setPrice(insertItemPrice6);
        insertItem6.setValue(insertItemValue6);
        insertItem6.setCollectionId(collection.getId());

        List<Long> ids = itemDao.insertItems(new Item[]{insertItem2, insertItem3, insertItem4, insertItem5, insertItem6});
        for (Long id: ids){
            assertNotEquals(-1, (long)id);
        }

        //Get Items By Collection
        List<Item> getItemsByCollection = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemsByCollection(collection.getId()));
        for(Item item : getItemsByCollection){
            assertEquals(collection.getId(), item.getCollectionId());
        }

        //Get Item By Id
        Item itemById = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemById(insertId));
        assertEquals(insertId, itemById.getId());

        //Update Item
        String updatedItemDescription = "A 2021 film on the origins of Shang Chi.";
        insertItem2.setId(ids.get(0));
        insertItem2.setDescription(updatedItemDescription);

        int updated = itemDao.updateItem(insertItem2);
        assertEquals(1, updated);

        //Update Items
        String updatedName = "Wonder Woman 1984";
        insertItem4.setName(updatedName);
        insertItem4.setId(ids.get(2));

        String updatedName2 = "Avengers: Endgame";
        insertItem3.setId(ids.get(1));
        insertItem3.setName(updatedName2);

        updated = itemDao.updateItems(new Item[] {insertItem3, insertItem4});
        assertEquals(2, updated);


        //Delete Item
        itemDao.deleteItem(insertItem2);
        itemById = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemById(insertItem2.getId()));
        assertNull(itemById);

        //Delete Items
        itemDao.deleteItems(new Item[]{insertItem3, insertItem4});
        itemById = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemById(insertItem3.getId()));
        assertNull(itemById);
        itemById = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemById(insertItem4.getId()));
        assertNull(itemById);

        //Delete Item By Id
        itemDao.deleteItemById(insertId);
        itemById = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemById(insertId));
        assertNull(itemById);

        //Delete Items From Collection
        itemDao.deleteItemsFromCollection(collection2.getId());
        getItemsByCollection = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemsByCollection(collection2.getId()));
        assertEquals(0, getItemsByCollection.size());

        //Delete All Items
        itemDao.deleteAllItems();
        getItemsByCollection = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemsByCollection(collection2.getId()));
        assertEquals(0, getItemsByCollection.size());
        getItemsByCollection = LiveDataTestUtil.getOrAwaitValue(itemDao.getItemsByCollection(collection2.getId()));
        assertEquals(0, getItemsByCollection.size());

        deleteCollections(collection,collection2);
    }

    private Collection insertCollection(String insertCollectionDescription, String collectionName) throws Exception{
        Collection insertCollection = new Collection();
        insertCollection.setDescription(insertCollectionDescription);
        insertCollection.setName(collectionName);

        long insertId = collectionDao.insertCollection(insertCollection);
        assertNotEquals(-1, insertId);

        Collection getCollectionById = LiveDataTestUtil.getOrAwaitValue(collectionDao.getCollectionById(insertId));
        assertEquals(insertId, getCollectionById.getId());

        return getCollectionById;
    }

    public void deleteCollections(Collection ... collections) throws Exception{
        collectionDao.deleteCollections(collections);
        List<Collection> getAllCollections = LiveDataTestUtil.getOrAwaitValue(collectionDao.getAllCollections());
        assertEquals(0, getAllCollections.size());
    }
}