package com.slalla.invenstory.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


import com.slalla.invenstory.database.dao.CollectionDao;
import com.slalla.invenstory.database.dao.ItemDao;
import com.slalla.invenstory.database.entity.Collection;
import com.slalla.invenstory.database.entity.Item;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Item.class, Collection.class}, version = 1, exportSchema = false)
public abstract class InvenstoryDatabase extends RoomDatabase {

    public abstract CollectionDao collectionDao();
    public abstract ItemDao itemDao();


    private static volatile InvenstoryDatabase INSTANCE;

    //Sets a finite number of threads to be able to be used by the Db
    private static final int NUMBER_OF_THREADS = 8;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };

    public static InvenstoryDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (InvenstoryDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            InvenstoryDatabase.class, "invenstory_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


}
