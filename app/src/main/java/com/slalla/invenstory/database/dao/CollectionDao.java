package com.slalla.invenstory.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.slalla.invenstory.database.entity.Collection;

import java.util.List;

@Dao
public interface CollectionDao {

    //Inserts
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertCollection(Collection collection);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    List<Long> insertCollections(Collection[] collections);

    //Deletes
    @Delete
    void deleteCollection(Collection collection);

    @Delete
    void deleteCollections(Collection[] collections);

    @Query("DELETE FROM COLLECTION")
    void deleteAllCollections();

    @Query("DELETE FROM COLLECTION WHERE id = :id")
    void deleteCollectionById(long id);

    //Updates
    @Update
    int updateCollection(Collection collection);

    @Update
    int updateCollections(Collection[] collections);

    //Queries
    @Query("SELECT * FROM COLLECTION")
    LiveData<List<Collection>> getAllCollections();

    @Query("SELECT * FROM COLLECTION WHERE id = :id")
    LiveData<Collection> getCollectionById(long id);

    @Query("SELECT * FROM COLLECTION WHERE name LIKE '%' || :search || '%'")
    LiveData<List<Collection>> getCollectionsWithNamesLike(String search);

    @Query("SELECT * FROM COLLECTION WHERE name LIKE :search || '%'")
    LiveData<List<Collection>> getCollectionsStartingWith(String search);

}
