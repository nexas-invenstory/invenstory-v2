package com.slalla.invenstory.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.slalla.invenstory.database.entity.Item;

import java.util.List;

@Dao
public interface ItemDao {

    //Inserts
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertItem(Item item);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    List<Long> insertItems(Item[] items);

    //Deletes
    @Delete
    void deleteItem(Item item);

    @Delete
    void deleteItems(Item[] items);

    @Query("DELETE FROM ITEM")
    void deleteAllItems();

    @Query("DELETE FROM ITEM WHERE id = :id")
    void deleteItemById(long id);

    @Query("DELETE FROM ITEM WHERE collection_id = :collectionId")
    void deleteItemsFromCollection(long collectionId);

    //Update
    @Update
    int updateItem(Item item);

    @Update
    int updateItems(Item[] items);

    //Queries
    @Query("SELECT * FROM ITEM WHERE collection_id = :collectionId")
    LiveData<List<Item>> getItemsByCollection(long collectionId);

    @Query("SELECT * FROM ITEM WHERE id = :id")
    LiveData<Item> getItemById(long id);
}
