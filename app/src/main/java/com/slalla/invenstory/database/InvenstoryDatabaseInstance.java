package com.slalla.invenstory.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.slalla.invenstory.database.dao.CollectionDao;
import com.slalla.invenstory.database.dao.ItemDao;
import com.slalla.invenstory.database.entity.Collection;
import com.slalla.invenstory.database.entity.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class InvenstoryDatabaseInstance {

    public CollectionDao collectionDao;
    public ItemDao itemDao;
    public ExecutorService executorService;

    public InvenstoryDatabaseInstance(Application application) {
        InvenstoryDatabase db = InvenstoryDatabase.getDatabase(application);
        collectionDao = db.collectionDao();
        itemDao = db.itemDao();
        executorService = InvenstoryDatabase.databaseWriteExecutor;
    }

    public long insertCollection(Collection collection){
        Callable<Long> insertCallable = () -> collectionDao.insertCollection(collection);
        long collectionId = 0;

        Future<Long> future = executorService.submit(insertCallable);
        try {
            collectionId = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return collectionId;
    }

    public List<Long> insertCollections(Collection[] collections){
        Callable<List<Long>> insertCallable = () -> collectionDao.insertCollections(collections);
        List<Long> collectionIds = new ArrayList<>();

        Future<List<Long>> future = executorService.submit(insertCallable);
        try {
            collectionIds = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return collectionIds;
    }

    public int updateCollection(Collection collection){
        Callable<Integer> updateCallable = () -> collectionDao.updateCollection(collection);

        int updated = 0;

        Future<Integer> future = executorService.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public int updateCollections(Collection[] collections){
        Callable<Integer> updateCallable = () -> collectionDao.updateCollections(collections);

        int updated = 0;

        Future<Integer> future = executorService.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void deleteCollection(Collection collection){
        executorService.execute(() -> {
            collectionDao.deleteCollection(collection);
        });
    }

    public void deleteCollections(Collection[] collections){
        executorService.execute(() ->{
            collectionDao.deleteCollections(collections);
        });
    }

    public void deleteAllCollections(){
        executorService.execute(() -> {
            collectionDao.deleteAllCollections();
        });
    }

    public void deleteCollectionsById(long id){
        executorService.execute(() ->{
            collectionDao.deleteCollectionById(id);
        });
    }

    public LiveData<Collection> getCollectionById(long id){
        return collectionDao.getCollectionById(id);
    }

    public LiveData<List<Collection>> getAllCollections(){
        return collectionDao.getAllCollections();
    }

    public LiveData<List<Collection>> getCollectionsStartingWith(String search){
        return collectionDao.getCollectionsStartingWith(search);
    }

    public LiveData<List<Collection>> getCollectionsWithNamesLike(String search){
        return collectionDao.getCollectionsWithNamesLike(search);
    }

    public long insertItem(Item item){
        Callable<Long> insertCallable = () -> itemDao.insertItem(item);

        long id = 0;

        Future<Long> future = executorService.submit(insertCallable);
        try {
            id = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return id;
    }

    public List<Long> insertItems(Item[] items){
        Callable<List<Long>> insertCallable = () -> itemDao.insertItems(items);

        List<Long> ids = new ArrayList<>();

        Future<List<Long>> future = executorService.submit(insertCallable);
        try {
            ids = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return ids;
    }

    public int updateItem(Item item){
        Callable<Integer> updateCallable = () -> itemDao.updateItem(item);

        int updated = 0;

        Future<Integer> future = executorService.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public int updateItems(Item[] items){
        Callable<Integer> updateCallable = () -> itemDao.updateItems(items);

        int updated = 0;

        Future<Integer> future = executorService.submit(updateCallable);
        try {
            updated = future.get();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return updated;
    }

    public void deleteItem(Item item){
        executorService.execute(() ->{
            itemDao.deleteItem(item);
        });
    }

    public void deleteItems(Item[] items){
        executorService.execute(()->{
            itemDao.deleteItems(items);
        });
    }

    public void deleteAllItems(){
        executorService.execute(()->{
            itemDao.deleteAllItems();
        });
    }

    public void deleteItemsByCollection(long id){
        executorService.execute(()->{
            itemDao.deleteItemsFromCollection(id);
        });
    }

    public void deleteItemById(long id){
        executorService.execute(()->{
            itemDao.deleteItemById(id);
        });
    }

    public LiveData<Item> getItemById(long id){
        return itemDao.getItemById(id);
    }

    public LiveData<List<Item>> getItemsByCollection(long id){
        return itemDao.getItemsByCollection(id);
    }

}
