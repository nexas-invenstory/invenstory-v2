package com.slalla.invenstory.database.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.slalla.invenstory.database.enums.Condition;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "item",
        foreignKeys = {
                @ForeignKey(entity = Collection.class, parentColumns = "id", childColumns = "collection_id", onDelete = CASCADE),
        }
)
public class Item {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long id;

    @ColumnInfo(name = "collection_id")
    private long collectionId;

    @ColumnInfo(name= "condition")
    private int conditionOrdinal;

    @Ignore
    private Condition condition;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "price")
    private float price;

    //    @ColumnInfo(name = "photos")
    @Ignore
    private String photos;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "value")
    private float value;

    public Item() {
    }

    public Item(long id, long collectionId, Condition condition, String name, float price, String photos, String description, float value) {
        this.id = id;
        this.collectionId = collectionId;
        this.condition = condition;
        this.conditionOrdinal = condition.ordinal();
        this.name = name;
        this.price = price;
        this.photos = photos;
        this.description = description;
        this.value = value;
    }

    public Item(long id, long collectionId, int conditionOrdinal, String name, float price, String photos, String description, float value) {
        this.id = id;
        this.collectionId = collectionId;
        this.conditionOrdinal = conditionOrdinal;
        this.condition = Condition.getCondition(conditionOrdinal);
        this.name = name;
        this.price = price;
        this.photos = photos;
        this.description = description;
        this.value = value;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getConditionOrdinal() {
        return conditionOrdinal;
    }

    public void setConditionOrdinal(int conditionOrdinal) {
        this.conditionOrdinal = conditionOrdinal;
        this.condition = Condition.getCondition(conditionOrdinal);
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
        this.conditionOrdinal = condition.ordinal();
    }

    public long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(long collectionId) {
        this.collectionId = collectionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
